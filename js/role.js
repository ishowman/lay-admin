layui.use(['element','jquery'],function(){
    var element = layui.element;

    var $ = layui.jquery;
    $('#abc').click(function(){
        addTab({ // 问题出在子页面无法访问父级页面的dom？
            tabTitle: $(this).text(),
            tabUrl: $(this).data('url'),
            tabId: $(this).prop('id') //选项卡标题的lay-id属性值
        })
    })
    function addTab(obj){
        if ($(".layui-tab-title li[lay-id=" + obj.tabId + "]").length) { // 如果已经存在则切换
            element.tabChange('tab-switch', obj.tabId);
        }else{ // 不存在则新增
            console.log(obj)
            element.tabAdd('tab-switch', {
                title: obj.tabTitle,
                content: '<iframe src='+ obj.tabUrl +' width="100%" style="min-height: 500px;" frameborder="0" scrolling="auto" onload="setIframeHeight(this)"></iframe>', // 选项卡内容，支持传入html
                id: obj.tabId //选项卡标题的lay-id属性值
            });
            element.tabChange('tab-switch', obj.tabId); //切换到新增的tab上
        }
    }
})
