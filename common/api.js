export default {
    login: 'user/login.do',
    customList: 'channel/getChannelInfoPage.do',
    flowList: 'money/getPartnerMoneyFlowPage.do'
}